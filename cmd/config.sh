cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    ds inject ubuntu-fixes.sh
    ds inject ssmtp.sh
    ds inject logwatch.sh $(hostname)
    ds inject sshtunnels.sh

    if [[ -n $DOMAIN ]]; then
        ds inject apache2-redirect-to-https.sh
        ds inject publish.sh
    fi
}
