cmd_publish_help() {
    cat <<_EOF
    ds publish <name.port>
        Start apache2 and publish the scripts of the directory
        'tunnels/<name.port>/'

    ds publish stop
        Stop apache2 and remove any published files.

_EOF
}

cmd_publish() {
    [[ -n $1 ]] || fail "Usage:\n$(cmd_publish_help)\n"
    if [[ $1 == 'stop' ]]; then
	_stop_publishing
    else
	_start_publishing "$@"
    fi
}

_stop_publishing() {
    ds exec systemctl stop apache2
    rm -rf published/*
}

_start_publishing() {
    local dir=$1
    [[ -d tunnels/$dir ]] \
        || fail "Directory 'tunnels/$dir' not found\n\nUsage:\n$(cmd_publish_help)\n"

    rm -rf published/$dir
    mkdir published/$dir

    local file1=$(ls tunnels/$dir/connect-*)
    local key1=$(tr -cd '[:alnum:]' < /dev/urandom | head -c30)
    cp -f $file1 published/$dir/connect-to-port-$key1

    local file2=$(ls tunnels/$dir/share-*)
    local key2=$(tr -cd '[:alnum:]' < /dev/urandom | head -c30)
    cp -f $file2 published/$dir/share-port-$key2

    ds exec systemctl start apache2

    ds exec tree -CA published/
    echo
    echo "wget -O $(basename $file1) https://$DOMAIN/$dir/connect-to-port-$key1"
    echo "wget -O $(basename $file2) https://$DOMAIN/$dir/share-port-$key2"
    echo
}

