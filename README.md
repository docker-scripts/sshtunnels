ssh tunnels
===========

These scripts help to create ssh tunnels between two computers that
don't have public IP.  One of them is called the **server** because it
shares a port through the ssh tunnel, and the other one is the
**client** because it connects to that port on the server through the
ssh tunnel. This is done with the help of a **proxy** server which has
a public IP, and can be seen from both parts, and acts as an
intermediary between them. These scripts are installed on the proxy
server.


```
                           PROXY_SSH_PORT=2221
                          +------------------+
            +------>----->|  PROXY_PORT=2475 |<-----<---+
            | +------<-----------<---------<----------+ |
            | | +-->----->|                  |<-----+ | |
            | | |         +------------------+      | | |
            | | |               PROXY               | | |
            | | |                                   | | |
            | | |                                   | | |
            | | | SERVER                     CLIENT | | |
     +------+ | +-----+                      +------+ | +------+
     | SERVER_PORT=22 |                      | CLIENT_PORT=2201|
     |  port-share.sh |                      |  port-connect.sh|
     +----------------+                      +-----------------+
                                         ssh -p 2201 user@localhost
```

For more details about sshtunnels see: http://dashohoxha.fs.al/sshtunnels/


## Installation

  - First install `ds`:
     + https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull sshtunnels`

  - Create a directory for the container: `ds init sshtunnels @sshtunnels`

  - Fix the settings: `cd /var/ds/sshtunnels/; vim settings.sh`

  - Build image, create the container and configure it: `ds make`


## Usage

  - Create tunnels:
    ```
    cd /var/ds/sshtunnels/
    ds tunnel-add server1 22
    ds tunnel-add server2 22
    ds tunnel-add server1 443
    ds tunnel-add server1 5900
    ```

  - Copy to **server1** the scripts
    `tunnels/server1.22/share-port-22.sh`,
    `tunnels/server1.443/share-port-443.sh` and
    `tunnels/server1.5900/share-port-5900.sh` and run them like this:
    ```bash
    chmod 700 share-port-22.sh
    ./share-port-22.sh
    chmod 700 share-port-443.sh
    ./share-port-443.sh
    chmod 700 share-port-5900.sh
    ./share-port-5900.sh
    ```
    Each of these scripts will open a ssh tunnel from the server side,
    and watch it, and try to keep it always open.  Copy also the
    script `tunnels/server2.22/share-port-22.sh` to **server2** and
    run it as well:
    ```bash
    chmod 700 share-port-22.sh
    ./share-port-22.sh
    ```

  - Copy to a client computer the scripts
    `tunnels/server1.22/connect-to-server1-22.sh`,
    `tunnels/server2.22/connect-to-server2-22.sh`, etc. and try them
    like this:
    ```bash
    chmod 700 connect-to-server1-22.sh
    ./connect-to-server1-22.sh 2201
    chmod 700 connect-to-server2-22.sh
    ./connect-to-server2-22.sh 2202

    ssh -p 2201 user1@localhost
    ssh -p 2202 user2@localhost
    ```
    The first commands will establish the ssh tunnel from the
    client side. Then, the following `ssh` commands will try to login
    through the ssh tunnel to the accounts **user1@server1** and
    **user2@server2**.

  - On the client run the script `./connect-to-server1-443.sh 444`, then
    open in a browser (on the client) `https://localhost:444`.

  - On the client run the script `./connect-to-server1-5900.sh 5900`,
    then open `localhost:5900` in a VNC viewer, which should actually
    connect to the port 5900 on **server1**.

  - To close and disable the tunnel from the server side run:
    ```bash
    ./share-port-22.sh stop
    ./share-port-443.sh stop
    ./share-port-5900.sh stop
    ```
    To close it from the client side run:
    ```bash
    ./connect-to-server1-22.sh stop
    ./connect-to-server2-22.sh stop
    ./connect-to-server1-443.sh stop
    ./connect-to-server1-5900.sh stop    
    ```

## Sharing scripts with apache2

If you have a real domain and assign it to the variable `DOMAIN` on
`settings.sh`, then you can publish and share the scripts through
apache2. Usually this requires `revproxy` to be installed as well:

+ https://gitlab.com/docker-scripts/revproxy#installation

To publish the scripts run:

```bash
ls tunnels/

ds publish server1.22
ds publish server2.22
ds publish server1.5900

ls -lR published/
```

This will start apache2 inside the container and make the scripts
available for download with an URL like:
https://sshtunnels.example.org/server1.22/connect-to-port-ljeQyEZEaZSEBrUpjxFQTAQFTFj4vQ
The random part to the URL is added in order to prevent the download
from unauthorized parties.

To stop publishing run:
```bash
ds publish stop
ls published/
```

This will stop apache2 and remove any published scripts.

