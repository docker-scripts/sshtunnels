APP=sshtunnels

PROXY_SERVER=sshtunnels.example.org    ### or IP of the proxy server
PROXY_SSH_PORT=22022

PORTS="$PROXY_SSH_PORT:22"

### Uncomment DOMAIN to enable file sharing through apache2.
#DOMAIN=sshtunnels.example.org
