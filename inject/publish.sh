#!/bin/bash

### create an apache2 config file
cat <<EOF >> /etc/apache2/conf-available/publish.conf
<Directory /var/www/html>
    Options -Indexes
</Directory>
EOF

### enable the config but don't start apache2
a2enconf publish
systemctl stop apache2
systemctl disable apache2
